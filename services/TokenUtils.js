import TokenService from "jwt-utils";
import { AsyncStorage } from "react-native";

const TokenUtils = new TokenService({
  storageSystem: AsyncStorage
});

export default TokenUtils;
