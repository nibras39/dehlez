import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  Image,
  View,
  AsyncStorage,
  TextInput,
  TouchableOpacity,
  Dimensions,
  Button
} from "react-native";
import axios from "axios";
import deviceStorage from "../services/deviceStorage";
//import TokenUtils from "../services/TokenUtils";
import DehleezLogo from "../images/Dl.png";
import googlebutton from "../images/googlebutton.png";
import gbutton from "../images/btngoogle.png";
import gb from "../images/gb.png";
import googbtn from "../images/btn_google_signin_light_normal_web.png";
import btn from "../images/btn.png";
import { SocialIcon } from "react-native-elements";
import fblogin from "../images/fbLogin.png";
import fbutton from "../images/fb log png.png";
import fbicon from "../images/fblog.png";

const { width: WIDTH } = Dimensions.get("window");

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      jwt: "",
      error: "",
      loading: false
    };
    this.loginUser = this.loginUser.bind(this);
    this.onLoginFail = this.onLoginFail.bind(this);
  }

  loginUser() {
    const { email, password } = this.state;

    var user = {
      email: email,
      password: password
    };

    var headers = { "Content-Type": "application/json" };

    this.setState({ error: "", loading: true });

    // NOTE Post to HTTPS only in production
    console.log(user);
    axios
      .post(
        "http://localhost:5000/api/users/login",
        {
          user: user
        },
        { headers: headers }
      )
      .then(response => {
        console.log(response);
        //const decodedToken = TokenUtils.decode(response.data.user.token);
        //console.log(decodedToken);
        deviceStorage.saveItem("id_token", response.data.user.token);
        //this.props.newJWT(response.data.user.token);
        //this.newJWT(response.data.user.token);
        this.props.navigation.navigate("App", {
          email: this.state.email
        });
      })
      .catch(error => {
        console.log(error.response);

        this.onLoginFail();
      });
  }

  onLoginFail() {
    this.setState({
      error: "Login Failed",
      loading: false
    });
  }
  render() {
    const { email, password } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.logocontainer}>
          <Image source={DehleezLogo} style={styles.logo} />

          <Text style={styles.logotext}>{"\nHello!"}</Text>
        </View>

        <View style={styles.form}>
          <View style={styles.inputcontainer}>
            <TextInput
              autoCapitalize="none"
              placeholder="Email"
              style={styles.input}
              onChangeText={email => this.setState({ email })}
            />
          </View>

          <View style={styles.inputcontainer}>
            <TextInput
              style={styles.input}
              placeholder="Password"
              onChangeText={password => this.setState({ password })}
              secureTextEntry="true"
            />
          </View>
        </View>

        <TouchableOpacity style={styles.logbutton} onPress={this.loginUser}>
          <Text style={styles.buttontext}>Sign in</Text>
        </TouchableOpacity>

        <View style={styles.create}>
          <TouchableOpacity style={styles.pass}>
            <Text>Create Account</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text>Forgot Password?</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.create}>
          <TouchableOpacity style={styles.fb}>
            <Image source={fbicon} style={styles.fbic} />
            <Text style={styles.textcol}>Sign in with facebook</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.fb}>
            <Image source={fbicon} style={styles.fbic} />
            <Text style={styles.textcol}>Sign in with facebook</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: null,
    height: null,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F8F9FE"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    borderRadius: 10,
    backgroundColor: "grey",
    marginBottom: 5
  },
  logo: {
    height: 160,
    width: 160,
    alignItems: "center"
  },
  logotext: {
    textAlign: "center",
    fontSize: 20,
    color: "#D3D3D3"
  },
  logocontainer: {
    marginBottom: 100
  },
  inputcontainer: {
    marginTop: 10
  },
  input: {
    width: WIDTH - 55,
    height: 60,
    borderRadius: 5,
    borderWidth: 1.5,
    borderColor: "#A9A9A9",
    paddingLeft: 45
  },
  logbutton: {
    width: WIDTH - 57,
    height: 60,
    borderRadius: 15,

    backgroundColor: "#EDB54D",

    marginTop: 30
  },
  buttontext: {
    marginTop: 15,
    color: "white",
    fontSize: 20,
    textAlign: "center"
  },
  form: {
    marginTop: -56
  },
  create: {
    //alignContent: "flex-start",
    flexDirection: "row",
    marginTop: 20
  },
  pass: {
    marginRight: 100
  },
  imgbutton: {
    height: 100,
    width: 100
  },
  socialbutton: {
    marginRight: 50,
    flexDirection: "row"
  },
  fb: {
    marginTop: 50,
    width: 150,
    height: 40,
    borderRadius: 3,

    backgroundColor: "#4D72A4"
  },
  textcol: {
    color: "white",
    paddingLeft: 10,
    marginTop: 9,
    fontSize: 11
  },
  fbic: {
    height: 30,
    width: 30,
    position: "absolute"
  }
});
