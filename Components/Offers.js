import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  View,
  AsyncStorage,
  TextInput,
  TouchableOpacity
} from "react-native";
import axios from "axios";

class Offers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      propertyID: "",
      bidderID: "",
      status: "",
      declinedReason: ""
    };

    this.createOffer = this.createOffer.bind(this);
  }

  createOffer() {
    const { propertyID, bidderID, status, declinedReason } = this.state;

    var offer = {
      propertyID: propertyID,
      bidderID: bidderID,
      status: status,
      declinedReason: declinedReason
    };

    var headers = { "Content-Type": "application/json" };

    axios
      .post(
        "http://localhost:5000/api/offers/createOffer",
        {
          offer: offer
        },
        { headers: headers }
      )
      .then(response => {
        console.log(offer);
        console.log(response);
        // this.props.navigation.navigate("Dash", {
        //   username: this.state.jwt
        // });
      })
      .catch(error => {
        console.log(error.response);
      });
  }

  render() {
    const { propertyID, bidderID, status, declinedReason } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.section}>
          <TextInput
            placeholder="Property ID"
            label="ID"
            value={propertyID}
            onChangeText={propertyID => this.setState({ propertyID })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="Bidder ID"
            label="Title"
            value={bidderID}
            onChangeText={bidderID => this.setState({ bidderID })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="status"
            label="status"
            value={status}
            onChangeText={status => this.setState({ status })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="Declining Reason"
            label="Decline Reason"
            value={declinedReason}
            onChangeText={declinedReason => this.setState({ declinedReason })}
          />
        </View>

        <TouchableOpacity onPress={this.createOffer}>
          <Text>Create Offer</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Offers;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  section: {
    flexDirection: "row",
    borderBottomWidth: 1,
    backgroundColor: "#fff",
    borderColor: "#ddd"
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
