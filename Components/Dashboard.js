import React, { Component } from "react";
import { Text, View, StyleSheet } from "react-native";

class Dashboard extends Component {
  render() {
    //const { navigation } = this.props;
    //const email = navigation.getParam("email");
    return (
      <View style={styles.container}>
        <Text>Hello </Text>
      </View>
    );
  }
}

export default Dashboard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
