import React, { Component } from "react";
import { Loading } from "../common/Loading";
import Registration from "../Registration";
import Login from "../Login";
import Dashboard from "../Dashboard";
import deviceStorage from "../../services/deviceStorage";

class Account extends Component {
  constructor() {
    super();
    this.state = {
      jwt: "",
      loading: true
    };
    this.newJWT = this.newJWT.bind(this);
    this.loadJWT = deviceStorage.loadJWT.bind(this);
    this.loadJWT();
  }

  newJWT(jwt) {
    this.setState({
      jwt: jwt
    });
  }

  render() {
    if (this.state.loading) {
      return <Loading size={"large"} />;
    } else if (!this.state.jwt) {
      return <Auth newJWT={this.newJWT()} />;
    } else if (this.state.jwt) {
      return <Dashboard deleteJWT={this.deleteJWT} />;
    }
  }
}

export default Account;
