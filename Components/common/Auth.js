import React, { Component } from "react";
import { View } from "react-native";
import Login from "../Login";

export default class Auth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showLogin: false
    };
    this.whichForm = this.whichForm.bind(this);
    this.authSwitch = this.authSwitch.bind(this);
  }

  whichForm() {
    if (!this.state.showLogin) {
      return (
        <Registration
          newJWT={this.props.newJWT()}
          authSwitch={this.authSwitch}
        />
      );
    } else {
      return <Login newJWT={this.props.newJWT} authSwitch={this.authSwitch} />;
    }
  }

  authSwitch() {
    this.setState({
      showLogin: !this.state.showLogin
    });
  }

  render() {
    return <View style={styles.container}>{this.whichForm()}</View>;
  }
}

const styles = {
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
};
