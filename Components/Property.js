import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  View,
  AsyncStorage,
  TextInput,
  TouchableOpacity
} from "react-native";
import axios from "axios";

class Property extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uploaderID: "",
      title: "",
      location: "",
      area: "",
      price: "",
      description: "",
      water: "",
      electricity: "",
      security: "",
      address: ""
    };

    this.createProperty = this.createProperty.bind(this);
  }

  createProperty() {
    const {
      uploaderID,
      title,
      location,
      area,
      price,
      description,
      water,
      electricity,
      security,
      address
    } = this.state;

    var property = {
      uploaderID: uploaderID,
      title: title,
      location: location,
      area: area,
      price: price,
      description: description,
      amenities: {
        water: water,
        electricity: electricity,
        security: security
      },
      address: address
    };

    var headers = { "Content-Type": "application/json" };

    axios
      .post(
        "http://localhost:5000/api/property/createProperty",
        {
          property: property
        },
        { headers: headers }
      )
      .then(response => {
        console.log(property);
        console.log(response);
        // this.props.navigation.navigate("Dash", {
        //   username: this.state.jwt
        // });
      })
      .catch(error => {
        console.log(error.response);
      });
  }

  render() {
    const {
      uploaderID,
      title,
      location,
      area,
      price,
      description,
      water,
      electricity,
      security,
      address
    } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.section}>
          <TextInput
            placeholder="ID"
            label="ID"
            value={uploaderID}
            onChangeText={uploaderID => this.setState({ uploaderID })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="Title"
            label="Title"
            value={title}
            onChangeText={title => this.setState({ title })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="location"
            label="location"
            value={location}
            onChangeText={location => this.setState({ location })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="Area"
            label="Area"
            value={area}
            onChangeText={area => this.setState({ area })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="Price"
            label="Price"
            value={price}
            onChangeText={price => this.setState({ price })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="Description"
            label="Description"
            value={description}
            onChangeText={description => this.setState({ description })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="water"
            label="water"
            value={water}
            onChangeText={water => this.setState({ water })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="electricity"
            label="Electricity"
            value={electricity}
            onChangeText={electricity => this.setState({ electricity })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="Security"
            label="Security"
            value={security}
            onChangeText={security => this.setState({ security })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="Address"
            label="Address"
            value={address}
            onChangeText={address => this.setState({ address })}
          />
        </View>

        <TouchableOpacity onPress={this.createProperty}>
          <Text>Create Property</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Property;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  section: {
    flexDirection: "row",
    borderBottomWidth: 1,
    backgroundColor: "#fff",
    borderColor: "#ddd"
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
