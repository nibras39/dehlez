import React, { Component } from "react";
import {
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Text,
  AsyncStorage
} from "react-native";
import axios from "axios";
import deviceStorage from "../services/deviceStorage";

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      firstName: "",
      lastName: "",
      password: "",
      phone: "",
      address: "",
      userRole: "User",
      jwt: "",
      error: ""
    };
    this.registerUser = this.registerUser.bind(this);
    this.onRegistrationFail = this.onRegistrationFail.bind(this);
  }

  registerUser() {
    const {
      email,
      firstName,
      lastName,
      password,
      userRole,
      phone,
      address
    } = this.state;

    var user = {
      email: email,
      firstName: firstName,
      lastName: lastName,
      password: password,
      userRole: "User",
      phone: phone,
      address: address
    };
    var headers = { "Content-Type": "application/json" };
    this.setState({ error: "", loading: true });

    console.log(user);
    axios
      .post(
        "http://localhost:5000/api/users/signUp",
        {
          user: user
        },
        { headers: headers }
      )
      .then(response => {
        deviceStorage.saveItem("id_token", response.data.user.token);
        this.props.newJWT(response.data.user.token);
        // this.props.navigation.navigate("Dash", {
        //   username: this.state.jwt
        // });

        console.log(response);
      })
      .catch(error => {
        console.log(error);
        this.onRegistrationFail();
      });
  }

  newJWT(jwt) {
    this.setState({
      jwt: jwt
    });
  }

  onRegistrationFail() {
    this.setState({
      error: "Registration Failed",
      loading: false
    });
  }

  render() {
    const { email, password, firstName, lastName, phone, address } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.section}>
          <TextInput
            placeholder="user@email.com"
            label="Email"
            value={email}
            onChangeText={email => this.setState({ email })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="First Name"
            label="First Name"
            value={firstName}
            onChangeText={firstName => this.setState({ firstName })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="Last Name"
            label="Last Name"
            value={lastName}
            onChangeText={lastName => this.setState({ lastName })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            secureTextEntry={true}
            placeholder="password"
            label="Password"
            value={password}
            onChangeText={password => this.setState({ password })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="Phone"
            label="Phone"
            value={phone}
            onChangeText={phone => this.setState({ phone })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="Address"
            label="Address"
            value={address}
            onChangeText={address => this.setState({ address })}
          />
        </View>

        <TouchableOpacity onPress={this.registerUser}>
          <Text>Register</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Registration;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  section: {
    flexDirection: "row",
    borderBottomWidth: 1,
    backgroundColor: "#fff",
    borderColor: "#ddd"
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
