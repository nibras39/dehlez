import React, { Component } from "react";
import {
  Text,
  StyleSheet,
  View,
  AsyncStorage,
  TextInput,
  TouchableOpacity
} from "react-native";
import axios from "axios";

class Visits extends Component {
  constructor(props) {
    super(props);
    this.state = {
      propertyID: "",
      ownerID: "",
      bidderID: "",
      agentID: "",
      time: "",
      date: ""
    };

    this.createVisit = this.createVisit.bind(this);
  }

  createVisit() {
    const { propertyID, ownerID, bidderID, agentID, time, date } = this.state;

    var visit = {
      propertyID: propertyID,
      ownerID: ownerID,
      bidderID: bidderID,
      agentID: agentID,
      time: time,
      date: date
    };

    var headers = { "Content-Type": "application/json" };

    axios
      .post(
        "http://localhost:5000/api/offers/createOffer",
        {
          visit: visit
        },
        { headers: headers }
      )
      .then(response => {
        console.log(visit);
        console.log(response);
        // this.props.navigation.navigate("Dash", {
        //   username: this.state.jwt
        // });
      })
      .catch(error => {
        console.log(error.response);
      });
  }

  render() {
    const { propertyID, ownerID, bidderID, agentID, time, date } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.section}>
          <TextInput
            placeholder="Property ID"
            label="ID"
            value={propertyID}
            onChangeText={propertyID => this.setState({ propertyID })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="Owner ID"
            label="Owner ID"
            value={ownerID}
            onChangeText={ownerID => this.setState({ ownerID })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="bidder id"
            label="Bidder ID"
            value={bidderID}
            onChangeText={bidderID => this.setState({ bidderID })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="Agent ID"
            label="Agent ID"
            value={agentID}
            onChangeText={agentID => this.setState({ agentID })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="Time"
            label="Time"
            value={time}
            onChangeText={time => this.setState({ time })}
          />
        </View>

        <View style={styles.section}>
          <TextInput
            placeholder="Date"
            label="Date"
            value={date}
            onChangeText={date => this.setState({ date })}
          />
        </View>

        <TouchableOpacity onPress={this.createVisit}>
          <Text>Create Visit</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Visits;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  section: {
    flexDirection: "row",
    borderBottomWidth: 1,
    backgroundColor: "#fff",
    borderColor: "#ddd"
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
