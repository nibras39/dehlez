/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import {
  createBottomTabNavigator,
  createAppContainer,
  createStackNavigator,
  createSwitchNavigator
} from "react-navigation";
import Login from "./Components/Login";
import Dashboard from "./Components/Dashboard";
import Registration from "./Components/Registration";
import Account from "./Components/common/Account";
import Property from "./Components/Property";
import Offers from "./Components/Offers";
import Visits from "./Components/Visits";

type Props = {};
class App extends Component<Props> {
  constructor() {
    super();
    this.state = {};
    this.newJWT = this.newJWT.bind(this);
  }

  newJWT(jwt) {
    this.setState({
      jwt: jwt
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Main Screen</Text>
      </View>
    );
  }
}

// const AppStack = createStackNavigator({ Dashboard: Dashboard });
// const AuthStack = createStackNavigator({
//   Login: Login,
//   Registration: Registration
// });
const AppNavigator = createBottomTabNavigator({
  Account: Login,
  Registration: Registration,
  Dashboard: Dashboard,
  Property: Property,
  Offers: Offers,
  Visits: Visits
});

export default createAppContainer(AppNavigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
